

    <!-- JAVASCRIPT -->
    <script src={{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('assets/libs/simplebar/simplebar.min.js') }}></script>
    <script src={{ asset('assets/libs/node-waves/waves.min.js') }}></script>
    <script src={{ asset('assets/libs/feather-icons/feather.min.js') }}></script>
    <script src={{ asset('assets/js/pages/plugins/lord-icon-2.1.0.js') }}></script>
    <script src={{ asset('assets/js/plugins.js') }}></script>

    <!--jquery cdn-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- apexcharts -->
    <script src={{ asset('assets/libs/apexcharts/apexcharts.min.js') }}></script>

    <!-- Dashboard init -->
    <script src={{ asset('assets/js/pages/dashboard-crm.init.js') }}></script>

    <!-- App js -->
    <script src={{ asset('assets/js/app.js') }}></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="{{ asset('assets/js/pages/select2.init.js') }}"></script>