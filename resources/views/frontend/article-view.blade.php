<!doctype html>
<html lang="en">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="icon" sizes="16x16" href="assets/img/favicon.png">

    <!-- Title -->
    <title> Article view </title>

    <!--Stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/line-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/swiper.min.css') }}">

    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/custom.css') }}">

</head>

<body>
    <!-- wrapper -->
    <div id="wrapper" class="wrapper">

        <!-- Header -->
        <header class="header fixed-top">
            <div class="header-main navbar-expand-xl">
                <div class="container-fluid">
                    <div class="header-main">
                        <!-- logo -->
                        <div class="site-branding">
                            <a class="dark-logo" href="index.html">
                                <img src="assets/img/logo/logo-dark.png" alt="">
                            </a>
                            <a class="light-logo" href="index.html">
                                <img src="assets/img/logo/logo-white.png" alt="">
                            </a>
                        </div><!--/-->

                        <div class="main-menu header-navbar">
                            <nav class="navbar">
                                <!--navbar-collapse-->
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ">
                                        <!--Homes -->
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('list') }}"> Home </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--/-->
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </header><!--/-->
        <main class="main">
            <!--post-default-->
            <section class="mt-60  mb-30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 side-content">
                            <div class="theiaStickySidebar">
                                <!--Post-single-->
                                <div class="post-single">
                                    <div class="post-single-image">
                                        <img src="{{ asset('storage/articles/' . $article->image) }}" alt="">
                                    </div>
                                    <div class="post-single-content">
                                        <h3 class="title">{{ $article->title }}</h3>
                                    </div>

                                    <div class="post-single-body">
                                        <p>
                                            {{ $article->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="widget mb-50">
                        <div class="widget-comments">
                            <div class="title">
                                <h5>Comments</h5>
                            </div>
                            <ul class="widget-comments-items">
                                @foreach ($article->comments as $comment)
                                    @if ($comment->parent_id == null)
                                        <li class="comment-item">
                                            <div class="content">
                                                <ul class="info list-inline">
                                                    <li>{{ $comment->name }}</li>
                                                    <li class="dot"></li>
                                                    <li>{{ Carbon\carbon::parse($comment->created_at)->format('d-m-Y') }}
                                                    </li>
                                                </ul>
                                                <p>
                                                    {{ $comment->comment }}
                                                </p>
                                                <div>
                                                    <a href="document:void()" class="btn-link"> <i
                                                            class="arrow_back"></i>Reply</a>
                                                </div>
                                                <div>
                                                    <form method="post" action="{{ route('comment.save', $article->id) }}">
                                                    @csrf
                                                    <input type="hidden" name="parent_id" value="{{ $comment->id }}">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <label class="text-dark" for="name"> name:</label>
                                                            <input type="text" class="form-control" id="name" name="name">
                                                            @error('name')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                            
                                                        <div class="col-6">
                                                            <label for="email" class="text-dark">Email:</label>
                                                            <input type="email" id="email" name="email" class="form-control">
                                                            @error('email')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                            
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <label for="comment" class="text-dark">Comment:</label>
                                                            <textarea name="comment" id="comment" rows="5" class="form-control"></textarea>
                                                            @error('comment')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                            
                                                    <div class="row">
                                                        <div class="mt-3">
                                                            <button type="submit" class="btn btn-primary">Submit Comment</button>
                                                        </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            @include('frontend.article-comment', ['childs' => $comment->replies])
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <h6 class="mb-1"> Add Comments</h6>

                    <form method="post" action="{{ route('comment.save', $article->id) }}">
                        @csrf

                        <div class="row">
                            <div class="col-3">
                                <label class="text-dark" for="name"> name:</label>
                                <input type="text" class="form-control" id="name" name="name">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-3">
                                <label for="email" class="text-dark">Email:</label>
                                <input type="email" id="email" name="email" class="form-control">
                                @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <label for="comment" class="text-dark">Comment:</label>
                                <textarea name="comment" id="comment" rows="5" class="form-control"></textarea>
                                @error('comment')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="mt-3">
                                <button type="submit" class="btn btn-primary">Submit Comment</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

        </main>



        <!--footer-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright">
                            <p>© Copyright 2021</p>
                        </div>
                        <div class="back">
                            <a href="post-default.html#" class="back-top">
                                <i class="fas fa-arrow-up"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

    <!--plugins -->
    <script src="{{ asset('frontend_assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/swiper.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/masonry.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/ajax-contact.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/switch.js') }}"></script>

    <!-- JS main  -->
    <script src="{{ asset('frontend_assets/js/main.js') }}"></script>

</body>

</html>
