<!doctype html>
<html lang="en">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="icon" sizes="16x16" href="assets/img/favicon.png">

    <!-- Title -->
    <title>Article list </title>
    
     <!--Stylesheets -->
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/bootstrap.min.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/all.min.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/line-awesome.min.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/swiper.min.css') }}">
 
     <!-- main style -->
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/style.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('frontend_assets/css/custom.css') }}">
 </head>
 
 <body>
     <!-- wrapper -->
    <div id="wrapper" class="wrapper">
 
          <!-- Header -->
        <header class="header fixed-top">
            <div class="header-main navbar-expand-xl">
                <div class="container-fluid">
                    <div class="header-main">
                        <!-- logo -->
                        <div class="site-branding">
                            <a class="dark-logo" href="index.html">
                                <img src="assets/img/logo/logo-dark.png" alt="">
                            </a>
                            <a class="light-logo" href="index.html">
                                <img src="assets/img/logo/logo-white.png" alt="">
                            </a>
                        </div><!--/-->
                        
                        <div class="main-menu header-navbar">
                            <nav class="navbar">
                            <!--navbar-collapse-->
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ">
                                    <!--Homes -->
                                    <li class="nav-item">
                                        <a class="nav-link active" href="{{ route('list') }}"> Home </a>
                                    </li>
                                </ul>
                            </div>
                            <!--/-->
                            </nav>
                        </div>
 
                    </div>
                </div> 
            </div>
        </header><!--/-->
    
        <main class="main">

            <!--blog-grid-->
            <section class="blog-grid mt-5">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-9 mt-30 side-content">
                            <div class="theiaStickySidebar">
                                <div class="row">
                                    @foreach ($articles as $article)
                                    <div class=" col-lg-6 col-md-6">
                                        <!--Post-1-->
                                        <div class="post-card">
                                            <div class="post-card-image">                                           
                                                    <img src="{{ asset('storage/articles/'.$article->image) }}">                                             
                                            </div>
                                            <div class="post-card-content">                                                          
                                                <small class="entry-title">
                                                   {{ Carbon\carbon::parse($article->created_at)->format('d-m-Y')}}
                                                </small>                                       
                                            </div>
                                            <div class="post-card-content">                                                          
                                                <h5 class="entry-title">
                                                   {{ $article->title }}
                                                </h5>
                                            </div>
                                            <div class="post-card-content">                                                          
                                                <span class="entry-title">                                                    
                                                        {{ Str::limit( $article->description, 100) }}                                                                                               
                                                </span>
                                            </div>
                                            <a href="{{ route('view', $article->id) }}"class="btn-primary"> Read More</a>
                                        </div>
                                        
                                        <!--/-->
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!--/-->
        </main>



        <!--footer-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright">
                            <p>© Copyright 2021</p>
                        </div>
                        <div class="back">
                            <a href="blog-grid.html#" class="back-top"><i class="fas fa-arrow-up"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
  
  
    <!--plugins -->
    <script src="{{ asset('frontend_assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/swiper.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/masonry.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/ajax-contact.js') }}"></script>
    <script src="{{ asset('frontend_assets/js/switch.js') }}"></script>
    
    <!-- JS main  -->
    <script src="{{ asset('frontend_assets/js/main.js') }}"></script>


</body>
</html>