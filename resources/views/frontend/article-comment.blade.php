@foreach ($childs as $child)
    <li class="comment-item" style="margin-left: 5rem;">
        <div class="content">
            <ul class="info list-inline">
                <li>{{ $child->name }}</li>
                <li class="dot"></li>
                <li>{{ Carbon\carbon::parse($child->created_at)->format('d-m-Y') }}
                </li>
            </ul>
            <p>
                {{ $child->comment }}
            </p>
            <div>
                <a href="document:void()" class="btn-link"> <i class="arrow_back"></i>Reply</a>
            </div>
            <div>
                <form method="post" action="{{ route('comment.save', $article->id) }}">
                    @csrf
                    <input type="hidden" name="parent_id" value="{{ $child->id }}">
                    <div class="row">
                        <div class="col-6">
                            <label class="text-dark" for="name"> name:</label>
                            <input type="text" class="form-control" id="name" name="name">
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-6">
                            <label for="email" class="text-dark">Email:</label>
                            <input type="email" id="email" name="email" class="form-control">
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <label for="comment" class="text-dark">Comment:</label>
                            <textarea name="comment" id="comment" rows="5" class="form-control"></textarea>
                            @error('comment')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="mt-3">
                            <button type="submit" class="btn btn-primary">Submit Comment</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('frontend.article-comment', ['childs' => $child->replies])
    </li>
@endforeach
