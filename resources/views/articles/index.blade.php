@extends('layouts.master')
@section('body')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">List Artcles</h4>
                        </div>
                        <a href="{{ route('articles.create') }}" class="btn btn-info add-btn"><i
                                class="ri-add-fill me-1 align-bottom"></i>Create Article</a>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row pt-3">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">List Artcles</h5>
                            </div>
                            <div class="card-body">
                                <table id="example"
                                    class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th data-ordering="false">SR No.</th>
                                            <th data-ordering="false">Name</th>
                                            <th data-ordering="false">Catgeory</th>
                                            <th data-ordering="false">Tags</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($articles as $article)
                                        @php
                                            $category = App\Models\Category::where('id', $article->cat_id)->first();
                                        @endphp
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $article->title }}</td>
                                                <td>{{ $category->name }}</td>
                                                <td>
                                                    @php
                                                         foreach( json_decode($article->tag_ids)  as $a_tag) {
                                                            $tag = App\Models\tags::where('id', $a_tag)->first();
                                                            echo '->' . $tag->name . '<br>';
                                                         }
                                                    @endphp
                                                   
                                                </td>
                                                <td>
                                                    <div class="dropdown d-inline-block">
                                                        <button class="btn btn-soft-secondary btn-sm dropdown"
                                                            type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill align-middle"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-end">
                                                            <li><a href="{{ route('articles.edit', $article->id) }}"
                                                                    class="dropdown-item"><i
                                                                        class="ri-eye-fill align-bottom me-2 text-muted"></i>
                                                                    Edit</a></li>
                                                            <li>
                                                                <form method="POST" action="{{ route('articles.destroy',$article->id) }}">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="dropdown-item remove-item-btn">
                                                                        <i
                                                                            class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>
                                                                        Delete
                                                                    </button>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    @include('layouts.footer')
@endsection
