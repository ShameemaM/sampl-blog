@extends('layouts.master')
@section('body')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Edit Article</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-xxl-12">
                        <div class="card">
                            <div class="card-header align-items-center d-flex">
                                <h4 class="card-title mb-0 flex-grow-1">Edit Article</h4>
                            </div><!-- end card header -->

                            <div class="card-body">
                                <div class="live-preview">
                                    <form method="POST" action="{{ route('articles.update', $article->id) }}"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('PATCH')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="Nameinput" class="form-label"> Title</label>
                                                    <input type="text" class="form-control" placeholder="Enter Title"
                                                        value="{{ $article->title }}" name="title">
                                                </div>
                                                @error('title')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="Nameinput" class="form-label">Select catgeoy</label>
                                                    <select class="form-control" name="category">
                                                        <option value="">--select--</option>
                                                        @foreach ($categories as $cat)
                                                            <option value="{{ $cat->id }}"> {{ $cat->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('category')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="Nameinput" class="form-label">Select Tags <small> (press
                                                            Ctrl to select multiple tags)</small></label>
                                                    <select class="form-control" name="tag[]" multiple>
                                                        @foreach ($tags as $tag)
                                                            <option value="{{ $tag->id }}"> {{ $tag->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('tag')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="Nameinput" class="form-label"> Description</label>
                                                    <textarea class="form-control" rows="5" placeholder="Enter description" name="description">
                                                        {{  $article->description }}
                                                    </textarea>
                                                </div>
                                                @error('description')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="Nameinput" class="form-label"> Image</label>
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                                @error('image')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-end">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    @include('layouts.footer')
@endsection
