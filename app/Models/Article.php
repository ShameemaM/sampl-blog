<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['*'];
    
    public function comments()
    {
       return $this->hasmany(Comment::class, 'article_id', 'id');
    }
}
