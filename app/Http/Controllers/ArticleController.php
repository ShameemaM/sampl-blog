<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articles=Article::where('user_id', Auth::id())->get();
        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::get();
        $tags = Tags::get();
        return view('articles.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'category' => 'required',
            'tag' => 'required'
        ]);

        if ($request->hasFile('image')) {
            $fileName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/articles', $fileName);
        } else {
            $fileName = '';
        }
        $article=new Article();
        $article->user_id = Auth::id();
        $article->title = $request->title;
        $article->cat_id = $request->category;
        $article->tag_ids = json_encode($request->tag);
        $article->description = $request->description;
        $article->image = $fileName;
        $article->save();
        return redirect()->route('articles.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $categories = Category::get();
        $tags = Tags::get();
        $article = Article::find($id);
        return view('articles.edit', compact('categories', 'tags', 'article'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            'tag' => 'required'
        ]);

        $article = Article::find($id);

        if ($request->hasFile('image')) {
            $fileName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/articles', $fileName);
        } else {
            $fileName = $article->image;
        }

        $article->title = $request->title;
        $article->cat_id = $request->category;
        $article->tag_ids = json_encode($request->tag);
        $article->description = $request->description;
        $article->image = $fileName;
        $article->save();
        return redirect()->route('articles.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $article=Article::find($id);
        $article->delete();
        return redirect()->route('articles.index');
    }
}
