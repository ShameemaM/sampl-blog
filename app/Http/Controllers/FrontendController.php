<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Reply;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function list()
    {
        $articles = Article::get();
        return view('frontend.article-list', compact('articles'));
    }

    public function view($id)
    {
        $article = Article::with(['comments' => function ($query) {
            $query->orderBy('id', 'desc');
        }])->find($id);
        // $article = Article::find($id);
        // $comments=Comment::where('article_id',$article->id)->orderBy('id', 'desc')->get();

        return view('frontend.article-view', compact('article'));
    }

    public function store(Request $request, $article_id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'comment' => 'required'
        ]);
        $comment = new Comment();
        $comment->article_id = $article_id;
        $comment->parent_id = $request->parent_id;
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->comment = $request->comment;
        $comment->save();
        return redirect()->back();
    }
}
