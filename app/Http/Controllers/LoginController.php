<?php

namespace App\Http\Controllers;

use App\Models\User;
use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');

    }
    public function signin(Request $req)
    {
       $this->validate($req, [
        'email'=> 'required|email', 
        'password'=>'required'
       ]);
       if (Auth::attempt(['email' => $req->email, 'password' => $req->password]))
        {
            return redirect()->route('home');
        } else {
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }  
    public function register()
    {
        return view('register');

    }
    public function submit(Request $req)
    {
       $this->validate($req, [
        'name'=>'required',
        'email'=> 'required|email|unique:users,email', 
        'password'=>'required|confirmed'
       ]);

       $user=new User();
       $user->name = $req->name;
       $user->email = $req->email;
       $user->password = Hash::make($req->password);
       $user->save();

       return redirect()->route('login');

    }  

}
