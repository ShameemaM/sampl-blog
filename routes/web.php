<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TagsController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/signin', [LoginController::class, 'signin'])->name('signin');
Route::any('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/register', [LoginController::class, 'register'])->name('register');
Route::post('/submit', [LoginController::class, 'submit'])->name('submit');

Route::get('/', [FrontendController::class, 'list'])->name('list');
Route::get('/view/{id}', [FrontendController::class, 'view'])->name('view');
Route::post('/comment/save/{article_id}', [FrontendController::class, 'store'])->name('comment.save');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('category', CategoryController::class);
    Route::resource('tags', TagsController::class);
    Route::resource('articles', ArticleController::class);
});
